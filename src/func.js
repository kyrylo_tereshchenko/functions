const getSum = (str1, str2) => {
  if ( typeof(str1)!='string' ||  typeof(str2)!='string'){
    return false;
}

 if ( /[a-z]/i.test(str1)  || /[a-z]/i.test(str2) ) {
    return false;
 }

 if (str2 == 0) {
   return str1;     
 } 
 else if (str1 == 0){
   return str2; 
 }  
 else   {
   while(str2!=0){
   var temp = str1 & str2; 
   str1 = str1 ^ str2;   
   str2 = temp<<1;  
  }

 }
      
 return str1.toString();
}

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  
  let countcomms = 0 ;
  let countposts = 0 ;

  for(let posts of listOfPosts){
      if( posts.author == authorName)  {
          countposts ++ ;

      }  
       for (let comms in posts.comments){
          if ( posts.comments[comms].author == authorName){
              countcomms++;
          }
      }
      
  }
  
 return 'Post:'+countposts+','+'comments:'+countcomms
}

const tickets=(people)=> {
  let cashbox = 0;
  let changecount =0;
  let change =0;
  let price = 25;
  if ( people[0]!=price){
    return 'NO';
  }
  
  cashbox = people[0];
  
  for(let i =1 ; i<people.length;i++){
    
      if (people[i]===price){
          cashbox+=people[i];
      }
      else{
          changecount = people[i]/price;
          change = price*(changecount-1);
          cashbox= cashbox - change;
          cashbox+= price;
      }
  }

  if (cashbox >= 0){
      return 'YES'
  }
  else {
      return 'NO';
  }

}


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
